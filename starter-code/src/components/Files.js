
import React, { Component } from "react";
import Filter from "./Filter";

class Files extends Component {
    state = {
        files: [],
        dwlIdFiles: [],
        fileWeigh: 0

    };

    getInfoFiles = file => {
        // console.log("Hello there I'm", file, "you are downloading")
        let files = this.props.files.filter(files => {
            return files.id === file.id
        })[0]

        if (files) {
            let fileWeigh = [...this.state.dwlIdFiles, files].map(file =>
                parseInt(file.size));

            let total = fileWeigh.reduce((one, two) => one + two);

            this.setState({
                files: [...this.state.files, files],
                dwlIdFiles: [...this.state.files, files],
                fileWeigh: total

            });
        }
    };



    render() {
        console.log("I'm the info of the file", this.state.files)
        return (
            <div className="showDwld">
                <ul>
                    {this.props.files.map(file => {
                        return (
                            <Filter
                                data={file}
                                showInfo={this.getInfoFiles} />
                        );
                    })}
                </ul>
                <div className="downloads">
                    <h1>Compress</h1>
                    <p>Total: {this.state.filesSize} </p>{/*Shows the number of how many files have we push the button */}
                    {this.state.files.map(file => {
                        return (

                            <div>
                                <p>{file.name}</p>{/*this comes from the JSON */}
                                <p>{file.size}</p>
                            </div>
                        )
                    })}

                </div>
            </div>
        );
    }
}

export default Files;