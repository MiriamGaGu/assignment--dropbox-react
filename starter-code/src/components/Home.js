import React, { Component } from 'react';
import Search from './Search'

class Home extends Component {
    render() {
        return (
            <div className="Home">
                <Search search={this.props.name} />
                <div className="results"></div>
            </div>

        )
    }

}

export default Home;