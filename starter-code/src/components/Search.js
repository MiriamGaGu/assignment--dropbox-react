import React, {Component} from 'react';
import Files from './Files';


class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            results: this.props.search
        };
    }

    filterByName = (e) => {

        let query = e.target.value.toLowerCase();

        let match = this.props.search.filter(function (item) {
            let nameInLowerCase = item.name.toLowerCase();

            return nameInLowerCase.includes(query);
        });

        this.setState({
            results: match
        });
    }


    render() {
        console.log(this.state.results)
        return (
            <React.Fragment>
                <input id="searcher" onChange={this.filterByName} type="search" className="search" placeholder="Search..." />
                <Files files={this.state.results} />
            </React.Fragment>
        )
    }

}

export default Search;
